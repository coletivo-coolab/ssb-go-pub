#!/usr/bin/env bash

# ssb
echo 'Starting ssb pub'
echo 'Cleaning up' # Necessary: https://github.com/cryptoscope/ssb/issues/124
rm -rf /root/.ssb-go/sublogs/shared-badger/
go-sbot &
sleep 10
# Envs
NAME="${PUB_NAME:=P2P Hotspot}"
ROOM_ADDR="${ROOM:=net:room.coolab.org:8008~shs:foHx1HuvaN++iCrQS+Zi916V6iNYEOtj9ZOAo+a0E+Q=}"
PUB_ADDR="${PUB:=net:pub.freesocial.co:8008~shs:ofYKOy2p9wsaxV73GqgOyh6C6nRGFM5FyciQyxwBd6A=}"
room_arr=(${ROOM_ADDR//":"/ })
ROOM_ID=${room_arr[3]}
pub_arr=(${PUB_ADDR//":"/ })
PUB_ID=${pub_arr[3]}
# Create invite
echo 'Posting ssb invite'
INVITE=($(sbotcli invite create --uses 9999))
IP=($(hostname -I | awk '{print $1;}'))
ID=($(sbotcli call whoami  | dasel -p json '.id' | tr -d '"'))
echo "{}" > /var/www/html/ssb.json
echo -e '<html>\n<html>\n\t<body>\n\t\t<p><a href="/ssb.json">SSB Pub Information</a></p><p><a href="/config.json">SSB Configs</a></p>\n\t</body>\n</html>' > /var/www/html/index.html
# Add id and invite info
dasel put object -f /var/www/html/ssb.json -t string -t string "pub" invite=$INVITE id=$ID
sed -i "s|\[::]|$IP|g" /var/www/html/ssb.json
# Add ssb config
cat /root/.ssb-go/config.toml | dasel -p toml -w json > /var/www/html/config.json

echo "Setting pub name to ${NAME}"
# Change name
sbotcli publish about --name "${NAME}" $ID
# TODO: Publish image
# cat some.json | sbotcli publish raw
# Connect to room
echo "Connecting to room"
sbotcli call conn.replicate "@${ROOM_ID}.ed25519"
sbotcli call conn.connect "$ROOM_ADDR"
echo "Connecting to pub"
sbotcli publish contact --following @${PUB_ID}.ed25519
sbotcli call conn.connect "$PUB_ADDR"
# apache
source /etc/apache2/envvars
a2enmod headers
/usr/sbin/apache2
sleep infinity
